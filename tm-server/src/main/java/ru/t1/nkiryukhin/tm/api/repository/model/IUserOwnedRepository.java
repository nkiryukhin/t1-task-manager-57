package ru.t1.nkiryukhin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId);

    List<M> findAll(@NotNull String userId, @Nullable Comparator comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

}
