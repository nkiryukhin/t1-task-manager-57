package ru.t1.nkiryukhin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public interface IProjectTaskDTOService {

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException;

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

}