package ru.t1.nkiryukhin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.nkiryukhin.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.nkiryukhin.tm.api.service.dto.ISessionDTOService;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionDTOService extends AbstractDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {

    @NotNull
    @Override
    protected ISessionDTORepository getRepository() {
        return context.getBean(ISessionDTORepository.class);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractFieldException {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(final List<SessionDTO> sessions) {
        for (final SessionDTO session : sessions) {
            removeOne(session);
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        SessionDTO session = findOneById(id);
        removeOne(session);
    }

    @Override
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        List<SessionDTO> sessions = findAll(userId);
        if (sessions == null || sessions.isEmpty()) return;
        remove(sessions);
    }

}
