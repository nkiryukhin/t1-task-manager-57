package ru.t1.nkiryukhin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.endpoint.IEndpoint;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.api.service.dto.*;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Getter
    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @Getter
    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Getter
    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @Getter
    @NotNull
    @Autowired
    private IUserDTOService userService;

    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IJmsLoggerService jmsLoggerService;

    @NotNull
    @Autowired
    private IConnectionService connectionService;

    @Getter
    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private IEndpoint[] endpoints;

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() throws AbstractException {
        @NotNull final UserDTO user = userService.create("user", "123", "user1@mail.com");
        @NotNull final UserDTO nkiryukhin = userService.create("nkiryukhin", "666", "na@mail.com");
        @NotNull final UserDTO admin = userService.create("admin", "sadmin", Role.ADMIN);

        projectService.add(user.getId(), new ProjectDTO("proj2", "project two", Status.IN_PROGRESS));
        projectService.add(nkiryukhin.getId(), new ProjectDTO("proj1", "project one", Status.COMPLETED));
        projectService.add(nkiryukhin.getId(), new ProjectDTO("proj4", "project four", Status.NOT_STARTED));
        projectService.add(admin.getId(), new ProjectDTO("proj3", "project three", Status.IN_PROGRESS));

        taskService.add(user.getId(), new TaskDTO("t2", "task two", Status.IN_PROGRESS));
        taskService.add(nkiryukhin.getId(), new TaskDTO("t1", "task one", Status.COMPLETED));
        taskService.add(nkiryukhin.getId(), new TaskDTO("t4", "task four", Status.NOT_STARTED));
        taskService.add(admin.getId(), new TaskDTO("t3", "task three", Status.IN_PROGRESS));
    }

    public void run() {
        initEndpoints();
        initPID();
        jmsLoggerService.initJmsLogger();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        try {
            initDemoData();
        } catch (Exception e) {
            loggerService.error(e);
        }
    }

    public void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}