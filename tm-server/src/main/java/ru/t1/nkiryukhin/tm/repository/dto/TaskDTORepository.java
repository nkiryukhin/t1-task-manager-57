package ru.t1.nkiryukhin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, TaskDTO.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) return 0;
        @NotNull final String jpql = "SELECT COUNT(m) FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class).setParameter("userId", userId).getSingleResult().intValue();
    }

}
