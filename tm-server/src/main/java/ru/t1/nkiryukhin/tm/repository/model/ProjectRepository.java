package ru.t1.nkiryukhin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.api.repository.model.IProjectRepository;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM Project m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public List<Project> findAll(@NotNull String userId, @Nullable Comparator comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, Project.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);

    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) return 0;
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class).setParameter("userId", userId).getSingleResult().intValue();
    }

}