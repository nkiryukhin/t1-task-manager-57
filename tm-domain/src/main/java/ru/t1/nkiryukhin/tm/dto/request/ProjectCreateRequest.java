package ru.t1.nkiryukhin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectCreateRequest extends AbstractUserRequest {

    @NotNull
    private String name;

    @NotNull
    private String description;

    public ProjectCreateRequest(@Nullable final String token) {
        super(token);
    }

}
